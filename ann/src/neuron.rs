use rand::distributions::{Distribution, Uniform};

#[derive(Default)]
pub struct Neuron {
    pub weights: Vec<f64>,
    pub net: f64,
    pub out: f64,
    pub sense: f64,
}

impl Neuron {
    pub fn builder(inputs_n: usize) -> NeuronBuilder {
        NeuronBuilder {
            n: inputs_n,
            range: (-1.0, 1.0),
        }
    }
}

pub struct NeuronBuilder {
    n: usize,
    range: (f64, f64),
}

impl NeuronBuilder {
    pub fn init_weights_range(&mut self, range: (f64, f64)) -> &mut Self {
        self.range = range;
        self
    }

    pub fn build(&self) -> Neuron {
        Neuron {
            weights: Uniform::new_inclusive(self.range.0, self.range.1)
                .sample_iter(&mut rand::thread_rng())
                .take(self.n + 1)
                .collect(),
            ..Default::default()
        }
    }
}
