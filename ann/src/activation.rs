pub fn logsig(x: f64) -> f64 {
    1.0 / (1.0 + (-x).exp())
}

pub fn logsig_deriv(x: f64) -> f64 {
    logsig(x) * (1.0 - logsig(x))
}
