use crate::activation::{logsig, logsig_deriv};
use crate::known::KnownTest;
use crate::neuron::Neuron;
use bincode::{deserialize, serialize};
use rand::{seq::SliceRandom, thread_rng};
use rayon::prelude::*;
use serde_derive::{Deserialize, Serialize};
use std::{cmp::Ordering, sync::Mutex};

#[derive(Default)]
struct Layer {
    neurons: Vec<Neuron>,
}

#[derive(Serialize, Deserialize)]
struct InternalState {
    scheme: Vec<usize>,
    weights: Vec<Vec<Vec<f64>>>,
    batch_size: usize,
    learning_rate: f64,
    err_tolerance: f64,
    epochs_limit: usize,
    epochs_run: usize,
    state: State,
    err: f64,
}

impl<'a> From<&ANN<'a>> for InternalState {
    fn from(item: &ANN<'a>) -> Self {
        Self {
            scheme: item.scheme.clone(),
            weights: item
                .layers
                .iter()
                .map(|l| l.neurons.iter().map(|n| n.weights.clone()).collect())
                .collect(),
            batch_size: item.batch_size,
            learning_rate: item.learning_rate,
            err_tolerance: item.err_tolerance,
            epochs_limit: item.epochs_limit,
            epochs_run: item.epochs_run,
            state: item.state,
            err: item.err,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Serialize, Deserialize, Debug)]
pub enum State {
    New,
    Training,
    Stopped,
}

#[derive(Debug)]
pub struct EpochResult {
    pub state: State,
    pub err: f64,
}

#[derive(Debug)]
pub struct TestResult {
    pub outputs: Vec<f64>,
    pub class: usize,
    pub class_val: f64,
}

pub struct ANN<'a> {
    pub scheme: Vec<usize>,
    layers: Vec<Layer>,
    pub training_set: &'a [KnownTest],
    fz: fn(f64) -> f64,
    fz_deriv: fn(f64) -> f64,
    pub batch_size: usize,
    pub learning_rate: f64,
    pub err_tolerance: f64,
    pub epochs_limit: usize,
    pub epochs_run: usize,
    pub state: State,
    pub err: f64,
}

impl<'a> ANN<'a> {
    pub fn builder(scheme: Vec<usize>, training_set: &'a [KnownTest]) -> ANNBuilder {
        ANNBuilder {
            scheme,
            training_set,
            init_weights_range: (-1.0, 1.0),
            fz: logsig,
            fz_deriv: logsig_deriv,
            batch_size: 32,
            learning_rate: 0.3,
            err_tolerance: 0.001,
            epochs_limit: 10000,
        }
    }

    pub fn from_bytes_builder(
        bytes: &[u8],
        training_set: &'a [KnownTest],
    ) -> bincode::Result<ANNFromBytesBuilder<'a>> {
        let internal_state: InternalState = deserialize(bytes)?;
        Ok(ANNFromBytesBuilder {
            scheme: internal_state.scheme,
            weights: internal_state.weights,
            training_set,
            fz: logsig,
            fz_deriv: logsig_deriv,
            batch_size: internal_state.batch_size,
            learning_rate: internal_state.learning_rate,
            err_tolerance: internal_state.err_tolerance,
            epochs_limit: internal_state.epochs_limit,
            epochs_run: internal_state.epochs_run,
            state: internal_state.state,
            err: internal_state.err,
        })
    }

    pub fn to_bytes(&self) -> bincode::Result<Vec<u8>> {
        let internal_state = InternalState::from(self);
        serialize(&internal_state)
    }

    pub fn run_training_epoch(&mut self) -> EpochResult {
        if self.state == State::Stopped {
            return EpochResult {
                state: State::Stopped,
                err: 0.0,
            };
        }

        let mut tests_indexes: Vec<usize> = (0..self.training_set.len()).collect();
        tests_indexes.shuffle(&mut thread_rng());
        for batch in tests_indexes.chunks(self.batch_size) {
            let mut grads = ANN::gradients_container(&self.layers);
            for test_index in batch {
                let test = &self.training_set[*test_index];
                ANN::acc_gradient(
                    &mut self.layers,
                    &mut grads,
                    &test.inputs,
                    &test.outputs,
                    self.fz,
                    self.fz_deriv,
                )
            }
            grads.par_iter_mut().for_each(|layer| {
                layer.par_iter_mut().for_each(|neuron| {
                    neuron.par_iter_mut().for_each(|w| {
                        *w /= batch.len() as f64;
                    })
                });
            });
            ANN::adjust_weights(&mut self.layers, &grads, self.learning_rate);
        }

        self.err = 0.0;
        for test in self.training_set.iter() {
            self.err += ANN::test_acc_squared_err(&mut self.layers, test, self.fz);
        }
        self.err /= self.training_set.len() as f64;
        self.epochs_run += 1;
        self.state = if self.epochs_run == self.epochs_limit || self.err <= self.err_tolerance {
            State::Stopped
        } else {
            State::Training
        };

        EpochResult {
            state: self.state,
            err: self.err,
        }
    }

    pub fn evaluate(&mut self, test: &KnownTest) -> TestResult {
        ANN::evaluate_internal(&mut self.layers, &test.inputs, self.fz);
        let outputs = self.layers[self.layers.len() - 1]
            .neurons
            .par_iter()
            .map(|n| n.out)
            .collect::<Vec<f64>>();
        let (class, &class_val) = outputs
            .iter()
            .enumerate()
            .max_by(|x, y| x.1.partial_cmp(y.1).unwrap_or(Ordering::Equal))
            .unwrap();
        TestResult {
            outputs,
            class,
            class_val,
        }
    }

    fn gradients_container(layers: &[Layer]) -> Vec<Vec<Vec<f64>>> {
        layers
            .iter()
            .map(|l| {
                l.neurons
                    .iter()
                    .map(|n| vec![0.0; n.weights.len()])
                    .collect()
            })
            .collect()
    }

    fn acc_gradient(
        layers: &mut [Layer],
        gradients: &mut [Vec<Vec<f64>>],
        inputs: &[f64],
        outputs: &[f64],
        fz: fn(f64) -> f64,
        fz_deriv: fn(f64) -> f64,
    ) {
        ANN::evaluate_internal(layers, inputs, fz);
        ANN::calc_grads(layers, gradients, inputs, outputs, fz_deriv);
    }

    fn evaluate_internal(layers: &mut [Layer], inputs: &[f64], fz: fn(f64) -> f64) {
        // First layer
        layers[0].neurons.par_iter_mut().for_each(|n| {
            n.net = -n.weights[0]
                + n.weights
                    .par_iter()
                    .enumerate()
                    .skip(1)
                    .map(|(i, w)| w * inputs[i - 1])
                    .sum::<f64>();
            n.out = fz(n.net);
        });
        // Remaining layers
        for m in 1..layers.len() {
            let (left, right) = layers.split_at_mut(m);
            let prev_layer = &mut left[left.len() - 1];
            let curr_layer = &mut right[0];
            curr_layer.neurons.par_iter_mut().for_each(|n| {
                n.net = -n.weights[0]
                    + n.weights
                        .par_iter()
                        .enumerate()
                        .skip(1)
                        .map(|(i, w)| w * prev_layer.neurons[i - 1].out)
                        .sum::<f64>();
                n.out = fz(n.net);
            });
        }
    }

    fn calc_grads(
        layers: &mut [Layer],
        gradients: &mut [Vec<Vec<f64>>],
        inputs: &[f64],
        outputs: &[f64],
        fz_deriv: fn(f64) -> f64,
    ) {
        let grad_mutex = Mutex::new(gradients);
        // Last layer
        {
            let last_layer_idx = layers.len() - 1;
            let (left, right) = layers.split_at_mut(last_layer_idx);
            let last_layer = &mut right[0];
            let prev_last_layer = &left[left.len() - 1];
            last_layer
                .neurons
                .par_iter_mut()
                .enumerate()
                .for_each(|(i, n)| {
                    n.sense = -2.0 * fz_deriv(n.net) * (outputs[i] - n.out);
                    let mut grads = grad_mutex.lock().unwrap();
                    grads[last_layer_idx][i][0] -= n.sense;
                    (1..n.weights.len()).for_each(|j| {
                        grads[last_layer_idx][i][j] += n.sense * prev_last_layer.neurons[j - 1].out;
                    });
                });
        }
        // Hidden layers
        for m in (1..layers.len() - 1).rev() {
            let (left, right) = layers.split_at_mut(m + 1);
            let (left_left, left_right) = left.split_at_mut(left.len() - 1);
            let prev_layer = &left_left[left_left.len() - 1];
            let curr_layer = &mut left_right[0];
            let next_layer = &right[0];
            curr_layer
                .neurons
                .par_iter_mut()
                .enumerate()
                .for_each(|(i, n)| {
                    n.sense = fz_deriv(n.net)
                        * next_layer
                            .neurons
                            .par_iter()
                            .map(|next| next.weights[i + 1] * next.sense)
                            .sum::<f64>();
                    let mut grads = grad_mutex.lock().unwrap();
                    grads[m][i][0] -= n.sense;
                    for j in 1..n.weights.len() {
                        grads[m][i][j] += n.sense * prev_layer.neurons[j - 1].out;
                    }
                });
        }
        // First layer
        let (left, right) = layers.split_at_mut(1);
        let first_layer = &mut left[0];
        let next_layer = &right[0];
        first_layer
            .neurons
            .par_iter_mut()
            .enumerate()
            .for_each(|(i, n)| {
                n.sense = fz_deriv(n.net)
                    * next_layer
                        .neurons
                        .par_iter()
                        .map(|next| next.weights[i + 1] * next.sense)
                        .sum::<f64>();
                let mut grads = grad_mutex.lock().unwrap();
                grads[0][i][0] -= n.sense;
                for j in 1..n.weights.len() {
                    grads[0][i][j] += n.sense * inputs[j - 1];
                }
            });
    }

    fn adjust_weights(layers: &mut [Layer], gradients: &[Vec<Vec<f64>>], learning_rate: f64) {
        layers.par_iter_mut().enumerate().for_each(|(i, l)| {
            l.neurons.par_iter_mut().enumerate().for_each(|(j, n)| {
                n.weights.par_iter_mut().enumerate().for_each(|(k, w)| {
                    *w -= learning_rate * gradients[i][j][k];
                });
            });
        });
    }

    fn test_acc_squared_err(layers: &mut [Layer], test: &KnownTest, fz: fn(f64) -> f64) -> f64 {
        ANN::evaluate_internal(layers, &test.inputs, fz);
        layers[layers.len() - 1]
            .neurons
            .par_iter()
            .enumerate()
            .map(|(i, n)| (test.outputs[i] - n.out).powi(2))
            .sum()
    }
}

pub struct ANNBuilder<'a> {
    scheme: Vec<usize>,
    training_set: &'a [KnownTest],
    init_weights_range: (f64, f64),
    fz: fn(f64) -> f64,
    fz_deriv: fn(f64) -> f64,
    batch_size: usize,
    learning_rate: f64,
    err_tolerance: f64,
    epochs_limit: usize,
}

impl<'a> ANNBuilder<'a> {
    pub fn init_weights_range(&mut self, range: (f64, f64)) -> &mut Self {
        self.init_weights_range = range;
        self
    }

    pub fn fz(&mut self, func: fn(f64) -> f64, derivative: fn(f64) -> f64) -> &mut Self {
        self.fz = func;
        self.fz_deriv = derivative;
        self
    }

    pub fn batch_size(&mut self, size: usize) -> &mut Self {
        self.batch_size = size;
        self
    }

    pub fn learning_rate(&mut self, rate: f64) -> &mut Self {
        self.learning_rate = rate;
        self
    }

    pub fn err_tolerance(&mut self, tolerance: f64) -> &mut Self {
        self.err_tolerance = tolerance;
        self
    }

    pub fn epochs_limit(&mut self, limit: usize) -> &mut Self {
        self.epochs_limit = limit;
        self
    }

    pub fn build(&self) -> ANN<'a> {
        let mut layers = Vec::new();
        for i in 1..self.scheme.len() {
            let mut new_layer = Layer::default();
            for _ in 0..self.scheme[i] {
                new_layer.neurons.push(
                    Neuron::builder(self.scheme[i - 1])
                        .init_weights_range(self.init_weights_range)
                        .build(),
                );
            }
            layers.push(new_layer);
        }
        ANN {
            scheme: self.scheme.clone(),
            layers,
            training_set: self.training_set,
            fz: self.fz,
            fz_deriv: self.fz_deriv,
            batch_size: self.batch_size,
            learning_rate: self.learning_rate,
            err_tolerance: self.err_tolerance,
            epochs_limit: self.epochs_limit,
            epochs_run: 0,
            state: State::New,
            err: 0.0,
        }
    }
}

pub struct ANNFromBytesBuilder<'a> {
    scheme: Vec<usize>,
    weights: Vec<Vec<Vec<f64>>>,
    training_set: &'a [KnownTest],
    fz: fn(f64) -> f64,
    fz_deriv: fn(f64) -> f64,
    batch_size: usize,
    learning_rate: f64,
    err_tolerance: f64,
    epochs_limit: usize,
    epochs_run: usize,
    state: State,
    err: f64,
}

impl<'a> ANNFromBytesBuilder<'a> {
    pub fn fz(&mut self, func: fn(f64) -> f64, derivative: fn(f64) -> f64) -> &mut Self {
        self.fz = func;
        self.fz_deriv = derivative;
        self
    }

    pub fn build(&self) -> ANN<'a> {
        ANN {
            scheme: self.scheme.clone(),
            layers: self
                .weights
                .iter()
                .map(|layer| Layer {
                    neurons: layer
                        .iter()
                        .map(|w_vec| Neuron {
                            weights: w_vec.to_vec(),
                            ..Neuron::default()
                        })
                        .collect(),
                })
                .collect(),
            training_set: self.training_set,
            fz: self.fz,
            fz_deriv: self.fz_deriv,
            batch_size: self.batch_size,
            learning_rate: self.learning_rate,
            err_tolerance: self.err_tolerance,
            epochs_limit: self.epochs_limit,
            epochs_run: self.epochs_run,
            state: self.state,
            err: self.err,
        }
    }
}
