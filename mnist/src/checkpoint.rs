use ann::{KnownTest, ANN};
use std::{
    error::Error,
    fs::File,
    io::{BufReader, Read, Write},
};

pub fn load<'a>(filename: &str, training_set: &'a [KnownTest]) -> Result<ANN<'a>, Box<Error>> {
    let mut checkpoint = BufReader::new(File::open(filename)?);
    let mut buffer = Vec::new();
    checkpoint.read_to_end(&mut buffer)?;
    Ok(ANN::from_bytes_builder(&buffer, training_set)?.build())
}

pub fn save(filename: &str, ann: &ANN) -> Result<(), Box<Error>> {
    File::create(filename)?.write_all(&ann.to_bytes()?)?;
    Ok(())
}
