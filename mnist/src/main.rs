mod checkpoint;
mod dataset;
mod test;
mod train;

use clap::App;
use std::error::Error;

fn main() -> Result<(), Box<Error>> {
    let cli_specs = clap::load_yaml!("cli.yaml");
    let app = App::from_yaml(cli_specs).get_matches();
    match app.subcommand() {
        ("train", Some(cmd)) => train::train_ann(cmd),
        ("test", Some(cmd)) => test::test_ann(cmd),
        _ => unimplemented!(),
    }
}
