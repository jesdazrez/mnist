use crate::{checkpoint, dataset::DataSet};
use ann::{scaler::Scaler, KnownTest, State, ANN};
use clap::ArgMatches;
use rayon::prelude::*;
use std::{error::Error, str::FromStr};

fn ann_from_cli<'a>(
    cli: &ArgMatches,
    training_set: &'a [KnownTest],
) -> Result<ANN<'a>, Box<Error>> {
    let mut builder = ANN::builder(vec![784, 20, 16, 10], training_set);
    if let Some(batch_size) = cli.value_of("batch-size") {
        builder.batch_size(usize::from_str(batch_size)?);
    }
    if let Some(learning_rate) = cli.value_of("learning-rate") {
        builder.learning_rate(f64::from_str(learning_rate)?);
    }
    if let Some(err_tolerance) = cli.value_of("err-tolerance") {
        builder.err_tolerance(f64::from_str(err_tolerance)?);
    }
    if let Some(epochs_limit) = cli.value_of("epochs-limit") {
        builder.epochs_limit(usize::from_str(epochs_limit)?);
    }
    Ok(builder.build())
}

pub fn train_ann(cmd: &ArgMatches) -> Result<(), Box<Error>> {
    // Loads dataset
    let mut dataset = DataSet::from_idx(
        cmd.value_of("images").unwrap(),
        cmd.value_of("labels").unwrap(),
    )?;
    // Normalizes dataset
    let scaler = Scaler::new(&dataset.samples);
    dataset
        .samples
        .par_iter_mut()
        .for_each(|t| scaler.transform_test(t));
    // Loads or creates ANN
    let checkpoint_arg = cmd.value_of("checkpoint");
    let mut ann = match checkpoint_arg {
        Some(filename) => checkpoint::load(filename, &dataset.samples)?,
        None => ann_from_cli(cmd, &dataset.samples)?,
    };
    // Loads out and save-each args
    let out_filename = checkpoint_arg.or_else(|| cmd.value_of("out")).unwrap();
    let save_each = usize::from_str(cmd.value_of("save-each").unwrap())?;
    let mut i = 0;
    while ann.state != State::Stopped {
        i += 1;
        let res = ann.run_training_epoch();
        if i == save_each {
            checkpoint::save(out_filename, &ann)?;
            println!(
                "Reached checkpoint after {} epochs with an error of {}",
                ann.epochs_run, res.err
            );
            i = 0;
        }
    }
    println!(
        "Training stopped after {} epochs with an error of {}",
        ann.epochs_run, ann.err
    );
    Ok(())
}
