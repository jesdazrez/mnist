use crate::{checkpoint, dataset::DataSet};
use ann::scaler::Scaler;
use clap::ArgMatches;
use std::{error::Error, str::FromStr};
use yansi::Paint;

pub fn test_ann(cmd: &ArgMatches) -> Result<(), Box<Error>> {
    let train_dataset =
        DataSet::from_idx("../train-images-idx3-ubyte", "../train-labels-idx1-ubyte")?;
    let scaler = Scaler::new(&train_dataset.samples);
    let mut test_dataset = DataSet::from_idx(
        cmd.value_of("images").unwrap(),
        cmd.value_of("labels").unwrap(),
    )?;
    let mut ann = checkpoint::load(cmd.value_of("ann").unwrap(), &train_dataset.samples)?;
    println!(
        "Testing with an error of {} after {} epochs",
        ann.err, ann.epochs_run
    );
    match cmd.value_of("index") {
        Some(idx_str) => {
            let idx = usize::from_str(idx_str)?;
            println!("Testing case at index {}:", idx);
            if !cfg!(windows) || Paint::enable_windows_ascii() {
                println!("\tVisual representation:");
                let row_size = (test_dataset.pixel_count as f64).sqrt() as usize;
                for row in test_dataset.samples[idx].inputs.chunks(row_size) {
                    print!("\t");
                    for pixel in row {
                        let val = *pixel as u8;
                        print!("{}", Paint::rgb(val, val, val, "*"));
                    }
                    println!();
                }
            }
            scaler.transform_test(&mut test_dataset.samples[idx]);
            let res = ann.evaluate(&test_dataset.samples[idx]);
            println!(
                "\tKnown class: {}",
                test_dataset.samples[idx].class().unwrap()
            );
            println!("\tApprox class: {} (~{})", res.class, res.class_val);
        }
        None => {
            let mut correct = 0;
            let mut incorrect = 0;
            let mut incorrect_cases = Vec::new();
            for (i, test) in test_dataset.samples.iter_mut().enumerate() {
                scaler.transform_test(test);
                let known_class = test.class().unwrap();
                let res = ann.evaluate(test);
                if res.class == known_class {
                    correct += 1;
                } else {
                    incorrect += 1;
                    incorrect_cases.push((i, known_class, res.class, res.class_val));
                }
            }
            println!(
                "Precision: {}",
                f64::from(correct) / f64::from(correct + incorrect) * 100.0
            );
            println!("Correct: {}", correct);
            println!("Incorrect: {}", incorrect);
            for (idx, known, approx, confidence) in incorrect_cases {
                println!(
                    "\t{:>5} -> Known: {}, Approx: {} (~{})",
                    idx, known, approx, confidence
                )
            }
        }
    }
    Ok(())
}
