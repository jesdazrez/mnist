use ann::KnownTest;
use byteorder::{BigEndian, ReadBytesExt};
use std::{fs::File, io::BufReader};

const LABELS_FILE_MAGIC_NUMBER: i32 = 0x0801;
const IMAGES_FILE_MAGIC_NUMBER: i32 = 0x0803;

pub struct DataSet {
    pub samples: Vec<KnownTest>,
    pub pixel_count: usize,
}

impl DataSet {
    // http://yann.lecun.com/exdb/mnist/ -> FILE FORMATS FOR THE MNIST DATABASE
    pub fn from_idx(images_filename: &str, labels_filename: &str) -> Result<Self, std::io::Error> {
        let mut images = BufReader::new(File::open(images_filename)?);
        let mut labels = BufReader::new(File::open(labels_filename)?);
        if images.read_i32::<BigEndian>()? != IMAGES_FILE_MAGIC_NUMBER {
            panic!("Unrecognized images file format!");
        }
        if labels.read_i32::<BigEndian>()? != LABELS_FILE_MAGIC_NUMBER {
            panic!("Unrecognized labels file format!");
        }
        let images_len = images.read_i32::<BigEndian>()?;
        let labels_len = labels.read_i32::<BigEndian>()?;
        if images_len != labels_len {
            panic!(format!(
                "Number of images ({}) does not match number of labels ({})!",
                images_len, labels_len
            ))
        }
        let pixel_count = images.read_i32::<BigEndian>()? * images.read_i32::<BigEndian>()?;
        let mut samples = Vec::new();
        for _ in 0..images_len {
            let mut known_test = KnownTest::new(Vec::new(), vec![0.0; 10]);
            for _ in 0..pixel_count {
                known_test.inputs.push(f64::from(images.read_u8()?));
            }
            known_test.outputs[usize::from(labels.read_u8()?)] = 1.0;
            samples.push(known_test);
        }
        Ok(DataSet {
            samples,
            pixel_count: pixel_count as usize,
        })
    }
}
